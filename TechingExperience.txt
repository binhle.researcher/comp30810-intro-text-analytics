A Summary of My Teaching Experiences in COMP30810, Intro to Text Analytics
====================================================================================================
Binh Thanh Le <thanhbinh.le@ucd.ie> 
School of Computer Science, UCD, 2018

I. A Description of the Course

	The subject matter in this course deals with the study of Text Analytics. After this course, students can:
		1. Have a clear understanding of the core concepts in text analytics and be familiar with a range of common analytics techniques in this area.
		2. Be familiar with common representations used to represent textual data.
		3. Have developed a knowledge of the main application areas in which these techniques prove useful.
		4. Be able to collect, pre-process and analyse textual datasets using Python.

II. A Schedule of the Course
	
	The course covers the concept of Text Analytics in three parts:
	1_ The introduction of Text Analytics vs Data Analytics
	2_ How to handle the text using the practical libraries: nltk, sklearn, gensim,  pandas
	3_ How to build the machine learning model: classification, clustering, regression models
	

	With 12 weeks, I split the content into:
		Week1 : Introduction and installation
		Week2 : Introduction Python programming
		Week3 : Loading Data and handle data with Pandas; first Analysis with Regular Expression
		Week4 : Data Preprocessing --> Tokens; visualisation Text with Wordcloud
		Week5 : Binary, Frequency, TF-IDF ; N-grams
		Week6 : Similarity using Euclidean, Cosine, Jaccard, Levenshtein Distance and Word Embedding(brief intro)
		Week7 : Text clustering: K-means, Hierarchical
		Week8 : Text classification: introduction, real-world applications; Comparison: supervised vs unsupervised, classification vs regression
		Week9 : Naive Bayes theorem; Classification with NB classifier
		Week10: Text classification with KNN; Linear Regression and Logistic Regression
		Week11: Text classification with Decision Tree and Random Forest
		Week12: extra lecture: Feature Selection/Extraction
		
		Note that: the cross-validation and validation metrics are included inside of classification lectures
		
	I organised two homeworks for students:
		HW1: The aims of this homework are about:
				a_ Understand how to handle from RAW text data to Tokens using some appropriate techniques: tokenization, remove punctuation, remove stopwords, ...
				b_ Be able to visualise the text data using wordclouds and plots
				c_ Some analyses such as: frequency, top 10 most common, n-grams
				d_ Understand and give the opinions on what they can see from analysis
				e_ Be able to use the similarities and wordnet to identify the topic of the source
				f_ be able to find the ODD news from the source
			DATA source: BBC news, topic: Technology
		HW2: The HW2 is a competition between teams in class. The aims of homework 2 are about:
				a_ Be able to reuse the understanding from homework1 to handle the RAW text
				b_ Be able to analyse further by cooperating with team
				c_ Practise classification with 10 classes data from BBC news
				d_ Can use the cross-validation to check the overfitting
				e_ Be able to produce the validation metric, such as: confusion matrix, f1, and ROC curves
		
	I also split the marking scheme into: 40% HW1 + 45% HW2 + 10% Discussion + 5% Attendance
		
III. Description of My teaching Method


Preliminary work
	- Set-up the teaching team
	- Provide the clear objective for the course
	- Set-up the rule for marking
	- Check the class-room to see what activities can be provided to students
	- Try to get the information of students before coming to class.

The Weekly Routine
	- Deliver the lectures and labs
	- Check the attendance and notice the students who absent mostly the module
	- Organize some activities in labs, such as: datathon, quiz, small test ...
	(note: from my observation, the students seem to like the activities in labs than the normal labs with code)
	- For the labs, I built the content with the presentation at first, then practice later
	- For the lectures, the previous lecture is summarized, then the problem is announced. Finally, I go to the details of lecture.
	(note: I will not provide the new thing if the old thing is not clear) 
	- Try to remember the name of students

The Factors to the Success or Failure of Learning
	1) I think the main factor to the success is the 'care'. I cared the students as much as I can, such as: send email to student if he/she absented more than 2 weeks.
	   After that I received the good feedback from that student; Ask students about how I can help them for the labs/homeworks.
	2) My failure on this course is about the schedule the marking scheme to students. 
	   I made the confused rule as first, I changed the rule lately, then I modified the rule again in a short time
	3) Another failure is about the homework. They are easy to get the high score.


IV. Extra Activities

	There are several interesting activities I made for the students:
		- Datathon: 
			+ Split the class into teams, which have one good student at least.
			+ Send them the data
			+ Give them at least 30' to 1h to read data, understand features and give opinions
			+ Conclude the best team, the good and not good ideas
		- Quiz:
			+ Split the class into teams, which have one good student at least.
			+ Open the organized questions, one minute for reading, 2 minutes for thinking and discussion.
			+ Marking the answers
		- Homework competition:
			+ Split teams
			+ Provide the training data + training class
			+ Ask them to predict the test data
			+ Best performance will win
		- Discussion point :
			+ Put the question and answer in forum to get the discussion point
			+ Make the presentation
			+ Make the report
		- Experience sharing:
			+ Organize the talking from senior to junior (Severin talked about how the research life)